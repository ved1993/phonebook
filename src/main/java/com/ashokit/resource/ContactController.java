package com.ashokit.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ashokit.entities.Contact;
import com.ashokit.exceptions.ContactNotCreatedException;
import com.ashokit.exceptions.ContactNotDeletedException;
import com.ashokit.exceptions.ContactNotExists;
import com.ashokit.exceptions.ContactNotUpdatedException;
import com.ashokit.service.ContactService;

@RestController
@RequestMapping("/api/contact")
public class ContactController {

	private final ContactService contactService;
	 
	public ContactController(ContactService contactService) {
		this.contactService = contactService;
	}

	@PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, 
				 consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<HttpStatus> saveContact(@RequestBody Contact contact){
			
		boolean createContact = contactService.createContact(contact);	
		
		if(!createContact) {
			throw new ContactNotCreatedException("Contact creation failed");
		}
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PutMapping(path="/{contactId}", 
				 produces = {MediaType.APPLICATION_JSON_VALUE}, 
				 consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<HttpStatus> updateContact(@PathVariable Integer contactId, @RequestBody Contact contact){
		 
			Optional<Contact> savedContact = contactService.getContactById(contactId);
		
			if(!savedContact.isPresent()) {
				throw new ContactNotExists("Contact Doesn't exists for contact id : "+ contactId);
			}	
		
			contact.setContactId(contactId); 
			boolean updateContact = contactService.updateContact(contact);
			
			if(!updateContact) {
				throw new ContactNotUpdatedException("Unable to Update Contact with Id : "+ contactId);
			}
			 
			return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@DeleteMapping(path="/{contactId}")
	public ResponseEntity<HttpStatus> deleteContact(@PathVariable Integer contactId){

		Optional<Contact> savedContact = contactService.getContactById(contactId);
		
		if(!savedContact.isPresent()) {
			throw new ContactNotExists("Contact Doesn't exists for contact id : "+ contactId);
		}	
		
		boolean deleteContact = contactService.deleteContact(contactId);
		
		if(!deleteContact) {
			throw new ContactNotDeletedException("Unable to Delete Contact with Id : "+ contactId);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping(produces= {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<Contact>> getContacts(){
		return new ResponseEntity<>(contactService.getContacts(), HttpStatus.OK);
	}
	
	@GetMapping(path="/{contactId}", produces= {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Contact> getContact(@PathVariable Integer contactId){
		return new ResponseEntity<Contact>(contactService.getContactById(contactId).get(), HttpStatus.OK);
	}
	
}
