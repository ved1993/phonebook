package com.ashokit.exceptions;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.ashokit.entities.HttpResponse;

@RestControllerAdvice
public class ExceptionHandling {
  
	@ExceptionHandler(ContactNotFoundException.class)
	public ResponseEntity<HttpResponse> contactNotFound(Exception e){	
		return createHttpResponse(HttpStatus.NOT_FOUND, e.getMessage());
	}
	
	@ExceptionHandler(ContactNotCreatedException.class)
	public ResponseEntity<HttpResponse> contactNotSaved(Exception e){	
		return createHttpResponse(HttpStatus.BAD_REQUEST, e.getMessage());
	}
	
	@ExceptionHandler(ContactNotUpdatedException.class)
	public ResponseEntity<HttpResponse> contactNotUpdated(Exception e){	
		return createHttpResponse(HttpStatus.BAD_REQUEST, e.getMessage());
	}
	
	@ExceptionHandler(ContactNotDeletedException.class)
	public ResponseEntity<HttpResponse> contactNotDeleted(Exception e){	
		return createHttpResponse(HttpStatus.BAD_REQUEST, e.getMessage());
	}
	
	@ExceptionHandler(ContactNotExists.class)
	public ResponseEntity<HttpResponse> contactNotExists(Exception e){	
		return createHttpResponse(HttpStatus.NOT_FOUND, e.getMessage());
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<HttpResponse> handleUnCaughtExceptions(Exception e){	
		return createHttpResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
	}
	
	private ResponseEntity<HttpResponse> createHttpResponse(HttpStatus httpStatus, String message){
		HttpResponse httpResponse = new HttpResponse(httpStatus.value(), httpStatus.getReasonPhrase().toUpperCase(), message.toUpperCase(), LocalDateTime.now());
		return new ResponseEntity<>(httpResponse, httpStatus);
	}
	
}
