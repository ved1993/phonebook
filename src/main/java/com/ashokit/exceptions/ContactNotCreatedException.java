package com.ashokit.exceptions;

public class ContactNotCreatedException extends RuntimeException{

	public ContactNotCreatedException(String message) {
		super(message);
	}
}
