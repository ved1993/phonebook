package com.ashokit.exceptions;

public class ContactNotDeletedException extends RuntimeException{

	public ContactNotDeletedException(String message) {
		super(message);
	}
}
