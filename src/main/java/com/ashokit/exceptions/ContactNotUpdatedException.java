package com.ashokit.exceptions;

public class ContactNotUpdatedException extends RuntimeException{
	
	public ContactNotUpdatedException(String message) {
		super(message);
	}
	
}
