package com.ashokit.exceptions;

public class ContactNotExists extends RuntimeException{
	public ContactNotExists(String message) {
		super(message);
	}
}
