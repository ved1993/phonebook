package com.ashokit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.ashokit.entities.Contact;
import com.ashokit.exceptions.ContactNotCreatedException;
import com.ashokit.exceptions.ContactNotDeletedException;
import com.ashokit.exceptions.ContactNotUpdatedException;
import com.ashokit.repository.ContactRepository;

@Service
public class ContactServiceImpl implements ContactService {

	private final ContactRepository contactRepo;
	
	public ContactServiceImpl(ContactRepository contactRepo) {
		this.contactRepo = contactRepo;
	}

	@Override
	public boolean createContact(Contact contact){
		
		try {
			contactRepo.save(contact);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}

	@Override
	public boolean updateContact(Contact contact){
		
		try {
			contactRepo.save(contact);	
			return true;
		}catch (Exception e) {
			return false;
		}
		
	}

	@Override
	public boolean deleteContact(Integer contactId){
		
		try {
			contactRepo.deleteById(contactId);						
			return true;
		}catch (Exception e) {
			return false;
		}
		
	}

	@Override
	public Optional<Contact> getContactById(Integer contactId) {

		 return contactRepo.findById(contactId);
	} 

	@Override
	public List<Contact> getContacts() {
		return contactRepo.findAll();
	}

}
