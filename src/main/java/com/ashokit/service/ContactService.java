package com.ashokit.service;

import java.util.List;
import java.util.Optional;
import com.ashokit.entities.Contact;

public interface ContactService {

	public boolean createContact(Contact contact);
	
	public boolean updateContact(Contact contact);
	
	public boolean deleteContact(Integer contactId);
	
	public Optional<Contact> getContactById(Integer contactId);
	
	public List<Contact> getContacts();
		
}
